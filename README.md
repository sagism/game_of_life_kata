# Game of life simulator kata template

 - Install: `npm install`
 - Run: `npx live-server`
 - Test: `npm test`

This sample uses the jest framework for testing.

You may need to install jest globally just so you can run `jest` from the command line, instead of running `./node-modules/jest-cli/bin/jest.js`: `npm install jest --global`

Note that you can edit the files right from the browser (in chrome): In the developer tools / sources / (+) Add folder to workspace

## Rules of the game:

   1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
   2. Any live cell with more than three live neighbours dies, as if by overcrowding.
   3. Any live cell with two or three live neighbours lives on to the next generation.
   4. Any dead cell with exactly three live neighbours becomes a live cell.
