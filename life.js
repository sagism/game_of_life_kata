export class World {

  constructor () {
    this.world = [];
      this.density = 0.1;
  }

  setRandomWorld(height=20, width=40, density=0.1) {
    this.world = [];
    for (let iheight=0; iheight<height; iheight++) {
        this.world[iheight] = [];
      for (let iwidth=0; iwidth<width; iwidth++) {
          this.world[iheight].push(Math.random() < density? 1:0);
      }
    }
  }

  get height() {
    return this.world.length;
  }

  get width() {
    return this.world[0].length;
  }

  next_day () {
    for (let iheight=0; iheight<this.height; iheight++) {
      for (let iwidth=0; iwidth<this.width; iwidth++) {
        this.world[iheight][iwidth] = ( Math.random() < this.density )? 1 : 0;
      }
    }
  }

  render (live='▪', dead=' ', separator='\n') {
      let repr = '';
      for (let iheight=0; iheight<this.height; iheight++) {
        for (let iwidth=0; iwidth<this.width; iwidth++) {
          repr += this.world[iheight][iwidth] ==  1 ? live : dead;
        }
        repr += separator;
      }
      return repr;
  }

  refresh() {
      this.next_day();
      return this.render()
  }
}
